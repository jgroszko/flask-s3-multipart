FROM python:3.6-alpine

COPY ./Pipfile* /app/
WORKDIR /app

RUN pip3 install --upgrade pip pipenv
RUN pipenv install --system --deploy
COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]

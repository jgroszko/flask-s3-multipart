# Flask S3 Multipart Upload

Quick demo for uploading large files to Amazon S3 with boto3, flask, and vanilla javascript. The javascript uses ES6 features and will only work on modern browsers.

## Using

- Make sure the CORS permissions on your S3 bucket are set properly.

- Set the following environment variables:

  - `AWS_ACCESS_KEY_ID`
  - `AWS_SECRET_ACCESS_KEY`
  - `AWS_S3_BUCKET`

## Acknowledgements

Borrowed heavily from [https://github.com/heryandi/flask-s3-multipart](https://github.com/heryandi/flask-s3-multipart)

(() => {
  const CHUNK_SIZE = 50 * 1024 * 1024;
  const uploadIdRegex = /<UploadId>(.*)<\/UploadId>/;
  
  const fileInput = document.getElementById('file-input');
  const fileProgressContainer = document.getElementById('file-progress-container');
  
  /*
   * Utilities
   */
  const md5 = (fileName, blob) => 
    new Promise((resolve) => {
      const fr = new FileReader();

      fr.addEventListener('loadend', () => {
        const sum = rstr2b64(rstr_md5(fr.result));
        resolve(sum);
      });

      fr.readAsBinaryString(blob);
    });

  const serial = (items, callback) =>
    items.reduce((promise, item) =>
      promise.then(async () => {
        await callback(item);
      }) 
      , Promise.resolve());

  /*
    * Progress Bar
    */ 

  const createProgress = fileName => {
    const progressElem = document.createElement('div');
    progressElem.classList.add('file-progress');

    const progressBarElem = document.createElement('div');
    progressBarElem.classList.add('file-progress-bar');
    progressBarElem.textContent = fileName;
    progressElem.appendChild(progressBarElem);

    fileProgressContainer.appendChild(progressElem);

    return progressBarElem;
  }

  const updateProgress = (progressBar, fileName, current, total) => {
    const percent = ((current/total) * 100).toFixed(2) + '%';

    progressBar.style.width = percent;

    const currentMb = (current / (1024 * 1024)).toFixed(2);
    const totalMb = (total / (1024 * 1024)).toFixed(2);

    progressBar.textContent = `${fileName} - ${percent} (${currentMb} MB of ${totalMb} MB)`;
  };

  const errorProgress = (progressBar, fileName, error) => {
    progressBar.classList.add('error');
    progressBar.textContent = `${fileName} - ${error}`;
  }

  const completeProgress = (progressBar, fileName) => {
    progressBar.style.width = "100%";
    progressBar.textContent = `${fileName} - Done`;
  }

  /*
   * Get Signed URLs
   */

  const getCreateUrl = async key => {
    const url = new URL('create_multipart_upload', window.location.href);
    url.searchParams.append('key', key);

    const response = await fetch(url);
    const { url: createUrl } = await response.json();

    return createUrl;
  };

  const getUploadPartUrl = async (key, uploadId, part_number, contentMd5, contentLength) => {
    const url = new URL('upload_part', window.location.href);
    url.searchParams.append('key', key);
    url.searchParams.append('upload_id', uploadId);
    url.searchParams.append('part_number', part_number);
    url.searchParams.append('content_md5', contentMd5);
    url.searchParams.append('content_length', contentLength);

    const response = await fetch(url);
    const { url: uploadPartUrl } = await response.json();

    return uploadPartUrl;
  };

  const getCompleteUrl = async (key, uploadId) => {
    const url = new URL('complete_multipart_upload', window.location.href);
    url.searchParams.append('key', key);
    url.searchParams.append('upload_id', uploadId);

    const response = await fetch(url);
    const { url: completeUrl } = await response.json();

    return completeUrl;
  };

  /*
   * Make S3 Requests
   */

  const createUpload = async url => {
    const response = await fetch(url, {
      method: 'POST',
    });

    if(response.status != 200) {
      throw "Create upload status code " + response.status;
    }

    const text = await response.text();
    return uploadIdRegex.exec(text)[1];
  }

  const uploadPart = (url, blob, contentMd5, progressCallback) => 
    new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.upload.onprogress = (event) => {
        const done = event.position || event.loaded;

        progressCallback(done);
      };

      xhr.onreadystatechange = (event) => {
        if (xhr.readyState == 4) {
          if(xhr.status == 200) {
            resolve(xhr.getResponseHeader('ETag'));
          } else {
            reject('Part upload status code ', xhr.status);
          }
        }
      };

      xhr.open('PUT', url, true);
      xhr.setRequestHeader('Content-MD5', contentMd5);
      xhr.send(blob);
    });

  const completeUpload = async (url, parts) => {
    let body = '<CompleteMultipartUpload xmlns="http://s3.amazonaws.com/doc/2006-03-01/">';
    Object.keys(parts).forEach(partNumber => {
      body += `
        <Part>
          <ETag>${parts[partNumber]}</ETag>
          <PartNumber>${partNumber}</PartNumber>
        </Part>
      `;
    });
    body += '</CompleteMultipartUpload>';

    const response = await fetch(url, {
      method: 'POST',
      body,
    });

    if(response.status != 200) {
      throw "Complete upload status code " + response.status;
    }
  };

  /*
   * Event Handlers
   */

  const onFileChange = async ({ target: { files } }) => {
    event.preventDefault();

    fileInput.disabled = true;

    serial(
      Array.from(files).map(file => ({
        file: file,
        progressBar: createProgress(file.name)
      })),
      async ({ file, progressBar }) => {
        try {
          const createUrl = await getCreateUrl(file.name);
          const uploadId = await createUpload(createUrl);

          const parts = {};
          let partNumber = 1;
          let i = 0;
          while (i < file.size) {
            updateProgress(progressBar, file.name, i, file.size);

            part = file.slice(i , i + CHUNK_SIZE);
            const contentLength = part.size;
            const contentMd5 = await md5(file.name, part);

            const partUrl = await getUploadPartUrl(
              file.name, uploadId, partNumber, contentMd5, contentLength
            );
            etag = await uploadPart(partUrl, part, contentMd5, partProgress => {
              updateProgress(progressBar, file.name, i + partProgress, file.size);
            });

            parts[partNumber] = etag;

            partNumber += 1;
            i += CHUNK_SIZE;
          }

          const completeUrl = await getCompleteUrl(file.name, uploadId);
          await completeUpload(completeUrl, parts);

          completeProgress(progressBar, file.name);

          fileInput.disabled = false;
        }
        catch (error) {
          errorProgress(progressBar, file.name, error);
        }
      });
  };

  fileInput.addEventListener('change', onFileChange);
})();
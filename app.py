import os
from flask import Flask, render_template, jsonify, request
import boto3

app = Flask(__name__)

EXPIRES_IN=1800

S3_BUCKET=os.environ['AWS_S3_BUCKET']

@app.route('/', methods=['GET'])
def index():
  return render_template(
    'index.html'
  )

@app.route('/create_multipart_upload', methods=['GET'])
def create_multipart_upload():
  key = request.args.get('key')

  s3_client = boto3.client('s3')

  url = s3_client.generate_presigned_url(
    ClientMethod='create_multipart_upload',
    Params={
      "Bucket": S3_BUCKET,
      "Key": request.args.get('key')
    },
    ExpiresIn=EXPIRES_IN,
    HttpMethod='POST'
  )

  return jsonify({
    "url": url
  })

@app.route('/upload_part')
def upload_part():
  key = request.args.get('key')
  part_number = int(request.args.get('part_number'))
  upload_id = request.args.get('upload_id')
  content_md5 = request.args.get('content_md5')
  content_length = int(request.args.get('content_length'))

  s3_client = boto3.client('s3')

  url = s3_client.generate_presigned_url(
    ClientMethod='upload_part',
    Params={
      "Bucket": S3_BUCKET,
      "Key": key,
      "PartNumber": part_number,
      "UploadId": upload_id,
      "ContentMD5": content_md5,
      "ContentLength": content_length,
    },
    ExpiresIn=EXPIRES_IN,
    HttpMethod="PUT"
  )

  return jsonify({
    "url": url
  })

@app.route('/complete_multipart_upload')
def complete_multipart_upload():
  key = request.args.get('key')
  upload_id = request.args.get('upload_id')

  s3_client = boto3.client('s3')

  url = s3_client.generate_presigned_url(
    ClientMethod='complete_multipart_upload',
    Params={
      "Bucket": S3_BUCKET,
      "Key": key,
      "UploadId": upload_id,
    },
    ExpiresIn=EXPIRES_IN,
    HttpMethod="POST"
  )

  return jsonify({
    "url": url
  })


if __name__ == "__main__":
  app.run(debug=True)
